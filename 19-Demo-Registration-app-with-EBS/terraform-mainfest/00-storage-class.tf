
resource "kubernetes_storage_class_v1" "mysql_sc" {
  metadata {
    name = "ebs-sc"
  }
  storage_provisioner = "ebs.csi.aws.com"
  reclaim_policy      = "Retain"
  parameters = {
    type = "gp3"
  }
allow_volume_expansion = true
volume_binding_mode = "WaitForFirstConsumer"
}