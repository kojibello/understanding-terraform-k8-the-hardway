
variable "vpc_cidr" {
  type        = string
  description = "Vpc CIDR"
}

variable "subnet_1_cidr" {
  type        = string
  description = "subnet 1 CIDR"
}

variable "subnet_2_cidr" {
  type        = string
  description = "subnet 2 CIDR"
}
