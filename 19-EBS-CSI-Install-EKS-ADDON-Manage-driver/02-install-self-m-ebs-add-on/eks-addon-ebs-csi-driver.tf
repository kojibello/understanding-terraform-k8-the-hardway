

# resource "helm_release" "ebs_csi_driver" {
#   depends_on = [ aws_iam_role_policy_attachment.ebs_csi_iam_role_policy_attach]

#   name       = "${var.cluster_name}-ebs-csi-driver"
#   repository = "https://kubernetes-sigs.github.io/aws-ebs-csi-driver"
#   chart      = "aws-ebs-csi-driver"
#   namespace = var.ebs_sci_namespace # MUST BE kube-system namespace

#   set {
#     name  = "image.repository"
#     value = "602401143452.dkr.ecr.us-east-1.amazonaws.com/eks/aws-ebs-csi-driver" # CHANGE REGION RESPECTIVELY https://docs.aws.amazon.com/eks/latest/userguide/add-ons-images.html 
#   }

#   set {
#     name  = "controller.serviceAccount.create"
#     value = "true"
#   }
#    set {
#     name  = "controller.serviceAccount.name"
#     value = var.service_account_name
#   }
#     set {
#     name  = "controller.serviceAccount.annotations.eks\\.amazonaws\\.com/role-arn" 
#     value = aws_iam_role.ebs_csi_iam_role.arn
#   }
# }

resource "aws_eks_addon" "ebs_eks_addon" {
  depends_on = [ aws_iam_role_policy_attachment.ebs_csi_iam_role_policy_attach]
  cluster_name =  var.cluster_name
  addon_name   = "aws-ebs-csi-driver" # kube-system
  service_account_role_arn  = aws_iam_role.ebs_csi_iam_role.arn
}
