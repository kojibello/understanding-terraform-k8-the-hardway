
resource "kubernetes_role_v1" "eksdeveloper_role" {
  metadata {
    name = "developer-role"
    namespace = "dev"
  }

  rule {
    api_groups     = ["", "apps", "extensions"]
    resources      = ["*"]
    verbs          = ["*"]
  }
  rule {
    api_groups = ["batch"]
    resources  = ["jobs", "cronjobs"]
    verbs      = ["*"]
  }
}