
### Command to Build Docker Images: 
```
docker login
docker build -t kojibello/kojitechs-registration-app-recap:v1.0.0 .
docker push kojibello/kojitechs-registration-app-recap:v1.0.0
```

#### MYSQL 
````
vversion: '3'
services:
  db:
    image: mysql
    volumes:
      - mysql-data:/var/lib/mysql
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: P1b7JBvP8II4
      MYSQL_DATABASE: webappdb
      MYSQL_USER: kojibello
      MYSQL_PASSWORD: P1b7JBvP8II4

registration-app:
    depends_on:
      - db
    image: ""
    restart: always
    ports:
      - "8080:8080"
    environment:
      DB_HOSTNAME: db
      DB_PORT: 3306
      DB_NAME: webappdb
      DB_USERNAME: kojibello
      DB_PASSWORD: P1b7JBvP8II4
volumes:
  mysql-data:


`````