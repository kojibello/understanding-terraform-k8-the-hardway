variable "cluster_name" {
  type        = string
  description = "EKS cluster name"
  default     = "kojietchs-eks-demo-cluster"
}

variable "image_version" {
  type        = string
  description = "container image version"
  default     = "f56f73ef6a4a7fb"
}

variable "db_username" {
  type        = string
  description = "(optional) describe your variable"
  default     = "kojitechs"
}

variable "db_name" {
  type        = string
  description = "value for database name"
  default     = "webappdb"
}

variable "port" {
  type        = number
  description = "database port"
  default     = 3306
}