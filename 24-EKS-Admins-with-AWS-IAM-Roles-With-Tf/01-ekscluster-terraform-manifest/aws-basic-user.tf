
resource "aws_iam_user" "basic_user" {
  name = "basicuser"
  path = "/"

  force_destroy = true
}

resource "aws_iam_user_policy" "basic_user_policy" {
  name = "basic-user-policy"
  user = aws_iam_user.basic_user.name

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "iam:ListRoles",
          "eks:*",
          "ssm:GetParameter"
        ]
        Effect   = "Allow"
        Resource = "${aws_eks_cluster.this.arn}"
      },
    ]
  })
}
