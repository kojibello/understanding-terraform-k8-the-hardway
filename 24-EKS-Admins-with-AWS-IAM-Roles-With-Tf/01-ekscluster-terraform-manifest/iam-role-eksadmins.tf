data "aws_caller_identity" "current" {}

resource "aws_iam_role" "eksadmin_role" {
  name = "eksadmin-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          AWS = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:root"
        }
      },
    ]
  })
  inline_policy {
    name = "eks-full-access-policy"

    policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
        {
            Action = [
                "iam:ListRoles",
                "eks:*", 
                "ssm:GetParameter"
            ]
            Effect   = "Allow"
            Resource = "${aws_eks_cluster.this.arn}"
        },
    ]
  })
  }
    tags = {
    Name = "eksadmin-role"
  }
}  

