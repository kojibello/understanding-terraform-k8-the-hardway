
vpc_cidr                  = "10.0.0.0/16"
public_subnet_cidr        = ["10.0.0.0/24", "10.0.2.0/24"]
dns_name                  = "kojitechs.click"
subject_alternative_names = ["*.kojitechs.click"]